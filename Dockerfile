# https://github.com/python-poetry/poetry/discussions/1879#discussioncomment-216865
FROM python:3.9-alpine as base

ENV PYTHONUNBUFFERED=1 \
    # prevents python creating .pyc files
    PYTHONDONTWRITEBYTECODE=1 \
    \
    # pip
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    \
    # poetry
    # make poetry install to this location
    POETRY_HOME="/opt/poetry" \
    # make poetry create the virtual environment in the project's root
    # it gets named `.venv`
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    # do not ask any interactive question
    POETRY_NO_INTERACTION=1 \
    \
    # paths
    # this is where our requirements + virtual environment will live
    PYSETUP_PATH="/opt/pysetup" \
    VENV_PATH="/opt/pysetup/.venv"

# prepend poetry and venv to path
ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

# `builder-base` stage is used to build deps + create our virtual environment
FROM base as builder-base
RUN apk update && apk add --no-cache \
    openssl-dev \
    musl-dev \
    libffi-dev \
    python3-dev \
    cargo \
    gcc \
    curl \ 
    libxslt-dev

# install poetry - respects $POETRY_VERSION & $POETRY_HOME
RUN curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python

# copy project requirement files here to ensure they will be cached.
WORKDIR $PYSETUP_PATH
COPY poetry.lock pyproject.toml ./

# install runtime deps - uses $POETRY_VIRTUALENVS_IN_PROJECT internally
RUN poetry install --no-dev

FROM base as production

RUN apk update && apk add --no-cache libxml2 libxslt
RUN pip install supervisor
COPY --from=builder-base $PYSETUP_PATH $PYSETUP_PATH
COPY supervisord.conf /etc/supervisord.conf

WORKDIR /usr/src/app
COPY ebooknotifier ./ebooknotifier

CMD ["/usr/local/bin/supervisord", "-c", "/etc/supervisord.conf"]