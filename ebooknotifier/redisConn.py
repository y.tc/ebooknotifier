import os

import redis
from dotenv import load_dotenv


class Conn:
    conn = None

    @classmethod
    def get(cls, db=None):
        global conn
        load_dotenv()
        if cls.conn is None:
            cls.conn = redis.ConnectionPool.from_url(
                url=os.getenv("REDIS_URL", "redis://localhost:6379/0")
            )
        return redis.Redis(connection_pool=cls.conn, db=db)
