#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

import ebooknotifier.Job

logger = logging.getLogger(__name__)


def main():
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    )

    ebooknotifier.Job.readmoo()
    ebooknotifier.Job.bookwalker()
