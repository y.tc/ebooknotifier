import logging
import time
from hashlib import blake2b
import os
from dotenv import load_dotenv

import rq
import telegram
from . import redisConn

load_dotenv()


class Item:
    def __init__(self, name, url, text):
        self.url = url
        self.name = name
        self.text = text
        h = blake2b(key=os.getenv("APPKEY", "APPKEY").encode(), digest_size=4)
        h.update(self.url.encode("utf-8"))
        self.hash = h.hexdigest().upper()

    def store(self):
        pass

    def exist(self):
        r = redisConn.Conn.get()
        if r.exists(self.url) > 0:
            logging.debug("URL: %s exists", self.url)
            r.expire(self.url, 2 * 24 * 60 * 60)
            return True
        else:
            logging.info(
                "URL: %s, hash %s doesn't exist",
                self.url,
                self.hash,
            )
            return False

    def publish(self):
        q = rq.Queue(connection=redisConn.Conn.get())
        q.enqueue(
            Item.send_msg,
            self.name,
            self.hash,
            self.url,
            "#" + self.hash + "\n" + self.text,
            retry=rq.Retry(max=3, interval=60),
        )

    @classmethod
    def send_msg(self, name, hash, url, text):
        logging.info("Publish #%s", hash)
        r = redisConn.Conn.get()
        if os.getenv("DB_WARMUP", "false").lower() in ("true", "1"):
            logging.info("setup DB record, but don't send")
        else:
            if r.exists(url) > 0:
                # TODO: reuse exist()
                logging.debug("URL: %s exists", url)
                return True
            bot = telegram.Bot(os.getenv("TG_BOT_TOKEN", ""))
            bot.send_message(
                os.getenv("TG_CHAT_ID", ""),
                "#" + name + "\n\n" + text + "\n\n" + url,
            )
        r.set(name=url, value="True", ex=2 * 24 * 60 * 60)
        time.sleep(5)
